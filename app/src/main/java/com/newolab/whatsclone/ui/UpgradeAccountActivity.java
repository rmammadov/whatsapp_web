package com.newolab.whatsclone.ui;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.newolab.whatsclone.R;
import com.newolab.whatsclone.helper.InAppBillingHelper;
import com.newolab.whatsclone.helper.SharedPreferecesHelper;
import com.newolab.whatsclone.ui.adapter.UpgradeAccountViewPagerAdapter;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

public class UpgradeAccountActivity extends AppCompatActivity implements View.OnClickListener, InAppBillingHelper.InAppBillingCallback {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private IndefinitePagerIndicator mIndefinitePagerIndicator;
    private AppCompatButton btnBuyPremium;

    private InAppBillingHelper mInAppBillingHelper = new InAppBillingHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_account);

        setUi();
    }

    private void setUi() {
        setTitle(getString(R.string.title_upgrade_account_activity));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = findViewById(R.id.view_pager);
        mIndefinitePagerIndicator = findViewById(R.id.view_pager_indicator);
        btnBuyPremium = findViewById(R.id.btn_buy_premium);

        btnBuyPremium.setOnClickListener(this);
        mPagerAdapter = new UpgradeAccountViewPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mIndefinitePagerIndicator.attachToViewPager(mPager);

        isPurchased();
    }

    private void isPurchased() {
        mInAppBillingHelper.mCallback = this;
        mInAppBillingHelper.setupBillingConnection(this, false);
    }

    private void onPurchased() {
        btnBuyPremium.setText(getString(R.string.btn_title_upgraded_account));
        btnBuyPremium.setClickable(false);
        setPro();
    }

    private void setPro() {
        SharedPreferecesHelper.setPro(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mInAppBillingHelper.endConnection();
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_buy_premium:
                mInAppBillingHelper.setupBillingConnection(this, true);
                break;
        }
    }

    @Override
    public void onPremiumPurchased(boolean status) {
        if(status) {
            onPurchased();
        }
    }
}
