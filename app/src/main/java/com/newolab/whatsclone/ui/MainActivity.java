package com.newolab.whatsclone.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.omadahealth.lollipin.lib.PinCompatActivity;
import com.github.omadahealth.lollipin.lib.managers.AppLock;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.newolab.whatsclone.R;
import com.newolab.whatsclone.helper.InAppBillingHelper;
import com.newolab.whatsclone.helper.SharedPreferecesHelper;
import com.newolab.whatsclone.service.HolderService;
import com.newolab.whatsclone.helper.FeedbackHelper;
import com.newolab.whatsclone.helper.SendFeedbackHelper;
import com.newolab.whatsclone.helper.ShareHelper;
import com.newolab.whatsclone.helper.UpdateHelper;
import com.newolab.whatsclone.util.Constants;

public class MainActivity extends PinCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, InAppBillingHelper.InAppBillingCallback {

    public static final int REQUEST_CODE_ENABLE = 111;

    private WebView webView;
    private boolean isVisible = false;
    private InterstitialAd mInterstitialAd;

    private static Activity mActivity;

    private static ValueCallback<Uri[]> mUploadMessageArr = null;
    private ValueCallback<Uri> mUploadMessage = null;
    private static final int REQUEST_SELECT_FILE = 1001;
    private static final int REQUEST_SELECT_FILE_LEGACY = 1002;

    private InAppBillingHelper mInAppBillingHelper = new InAppBillingHelper();

    public static MainActivity getInstance() {
        return (MainActivity) mActivity;
    }

    /**
     * Back handler
     */
    class BackHandler implements Runnable {
        final /* synthetic */ MainActivity activity;

        BackHandler(MainActivity mainActivity) {
            this.activity = mainActivity;
        }

        public void run() {
            if (this.activity.mInterstitialAd.isLoaded() && !SharedPreferecesHelper.isAdsRemoved(mActivity)) {
                this.activity.mInterstitialAd.show();
            } else {
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
            this.activity.isVisible = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_main);

        mActivity = this;

        setUi();
    }

    private void setUi() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentStorySaver = new Intent(getApplicationContext(), StorySaverActivity.class);
                startActivity(intentStorySaver);
            }
        });

        setWebView();
        isPurchased();
        setAds();
        showRatingDialog();
        showUpdateDialog();
    }

    private void setWebView() {
        if (HolderService.newInstance().getWebView() == null) {
            this.webView = HolderService.newInstance().initWebView((Activity) this);
        } else {
            this.webView = HolderService.newInstance().getWebView();
            if (this.webView.getParent() != null) {
                ((ViewGroup) this.webView.getParent()).removeView(this.webView);
            }
        }
        HolderService.newInstance().setWebView(this.webView);
        ((LinearLayout) findViewById(R.id.ll_root)).addView(this.webView);
    }

    private void isPurchased() {
        mInAppBillingHelper.mCallback = this;
        mInAppBillingHelper.setupBillingConnection(this, false);
    }

    private void upgradeToPremiumAccount() {
        Intent intentUpgrade = new Intent(this, UpgradeAccountActivity.class);
        startActivity(intentUpgrade);
    }

    private void removeAds() {
        if (SharedPreferecesHelper.isPro(this)) {
            SharedPreferecesHelper.setAdsRemoved(this, true);
            Toast.makeText(this, "You are pro and Ads is turned off for you!", Toast.LENGTH_SHORT).show();
        } else {
            upgradeToPremiumAccount();
        }
    }

    private void setPassword() {
        if (SharedPreferecesHelper.isPro(this)) {
            Intent intent = new Intent(MainActivity.this, SetPinActivity.class);
            intent.putExtra(AppLock.EXTRA_TYPE, AppLock.ENABLE_PINLOCK);
            startActivityForResult(intent, REQUEST_CODE_ENABLE);
        } else {
            upgradeToPremiumAccount();
        }
    }

    private void showUpdateDialog() {
        UpdateHelper.checkUpdate(this);
    }

    private void showRatingDialog() {
        FeedbackHelper.showFeedbackDialogBasedSessions(this);
    }

    private void showShareDialog() {
        ShareHelper.shareApp(this);
    }

    private void sendFeedback() {
        SendFeedbackHelper.sendViaMail(this);
    }

    @Override
    public void onPremiumPurchased(boolean status) {
        if(status) {
            setPro();
        }
    }

    private void setPro() {
        SharedPreferecesHelper.setPro(this);
    }

    /**
     * Showing Ads
     */
    private void setAds() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(Constants.ADMOB_AD_UNIT_ID);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                // Proceed to the next level.
                loadNewAds();
            }
        });

        loadNewAds();
    }

    private void loadNewAds() {
        if (!SharedPreferecesHelper.isAdsRemoved(this) && mInterstitialAd != null && mInterstitialAd.getAdUnitId() != null) {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (this.webView == null) {
            super.onBackPressed();
        } else if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!getFragmentManager().popBackStackImmediate()) {
            if (this.isVisible) {
                super.onBackPressed();
                return;
            }
            this.isVisible = true;
            this.webView.loadUrl("javascript:(function(){document.getElementsByClassName('_1Iexl')[1].style.flexBasis='0%';document.getElementsByClassName('k1feT')[1].style.flexBasis='100%';})()");
            new Handler().postDelayed(new BackHandler(this), 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reload) {
            webView.reload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_upgrade) {
            upgradeToPremiumAccount();
        } else if (id == R.id.nav_remove_ads) {
            removeAds();
        } else if (id == R.id.nav_set_password_lock) {
            setPassword();
        } else if (id == R.id.nav_share) {
            showShareDialog();
        } else if (id == R.id.nav_rate) {
            FeedbackHelper.showFeedbackDialogInstant(this);
        } else if (id == R.id.nav_send_feedback) {
            sendFeedback();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        onBackPressed();
        return true;
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onStop() {
        super.onStop();
        startService(new Intent(this, HolderService.class));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mInAppBillingHelper.endConnection();
    }

    @SuppressLint({"NewApi"})
    public boolean startFileChooserIntent(ValueCallback<Uri[]> filePathCallback, Intent intentChoose) {
        if (mUploadMessageArr != null) {
            mUploadMessageArr.onReceiveValue(null);
            mUploadMessageArr = null;
        }
        mUploadMessageArr = filePathCallback;
        try {
            mActivity.startActivityForResult(intentChoose, REQUEST_SELECT_FILE, new Bundle());
            return true;
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            if (mUploadMessageArr != null) {
                mUploadMessageArr.onReceiveValue(null);
                mUploadMessageArr = null;
            }
            return false;
        }
    }

    @SuppressLint({"NewApi", "Deprecated"})
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_FILE_LEGACY) {
            if (this.mUploadMessage != null) {
                Uri result = (data == null || resultCode != -1) ? null : data.getData();
                this.mUploadMessage.onReceiveValue(result);
                this.mUploadMessage = null;
            }
        } else if (requestCode == REQUEST_SELECT_FILE && Build.VERSION.SDK_INT >= 21 && mUploadMessageArr != null) {
            mUploadMessageArr.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
            mUploadMessageArr = null;
        }
    }
}
