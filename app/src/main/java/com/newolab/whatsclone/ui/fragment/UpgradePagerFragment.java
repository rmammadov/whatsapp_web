package com.newolab.whatsclone.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.newolab.whatsclone.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UpgradePagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UpgradePagerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "text";
    private static final String ARG_PARAM2 = "icon";

    // TODO: Rename and change types of parameters
    private int mTextId;
    private int mIconId;

    private View view;
    private AppCompatImageView ivIcon;
    private AppCompatTextView tvText;


    public UpgradePagerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param textId Parameter 1.
     * @param iconId Parameter 2.
     * @return A new instance of fragment UpgradePagerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UpgradePagerFragment newInstance(int textId, int iconId) {
        UpgradePagerFragment fragment = new UpgradePagerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, textId);
        args.putInt(ARG_PARAM2, iconId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTextId = getArguments().getInt(ARG_PARAM1);
            mIconId = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_upgrade_pager, container, false);

        setUi();

        return view;
    }

    private void setUi() {
        ivIcon = view.findViewById(R.id.image_view_icon_upgrade_fragment);
        tvText = view.findViewById(R.id.tv_description_upgrade_fragment);

        loadData();
    }

    private void loadData() {
        ivIcon.setImageResource(mIconId);
        tvText.setText(getActivity().getString(mTextId));
    }
}
