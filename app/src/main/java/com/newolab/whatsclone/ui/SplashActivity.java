package com.newolab.whatsclone.ui;

import android.Manifest;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.newolab.whatsclone.R;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.List;

import static com.newolab.whatsclone.util.Constants.SPLASH_DISPLAY_LENGTH;

public class SplashActivity extends AppCompatActivity implements InternetConnectivityListener {

    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    private AppCompatImageView ivLogo;
    private AppCompatTextView tvCompanyName;
    private AppCompatTextView tvStatus;
    private Boolean isPermissionsGranted = false;
    private Boolean isInternetConnected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUi();
    }

    private void setUi() {
        ivLogo = findViewById(R.id.img_splash_logo);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
        tvCompanyName = findViewById(R.id.tv_company_name);
        tvStatus = findViewById(R.id.tv_status);

        Glide.with(this)
                .load(R.drawable.ic_logo_whatsweb)
                .into(ivLogo);

        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            isPermissionsGranted = true;
                        } else {
                            isPermissionsGranted = false;
                        }
                        onInternetConnectivityChanged(isInternetConnected);
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                    }
                }).check();
    }

    @Override
    protected void onDestroy() {
        mInternetAvailabilityChecker
                .removeInternetConnectivityChangeListener(this);
        super.onDestroy();
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        isInternetConnected = isConnected;
        if(isConnected && isPermissionsGranted) {
            tvStatus.setVisibility(View.GONE);
            tvCompanyName.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                    /* Create an Intent that will start the Main-Activity. */
                    Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        } else if(!isConnected) {
            tvStatus.setText(getString(R.string.text_no_internet));
            tvCompanyName.setVisibility(View.GONE);
            tvStatus.setVisibility(View.VISIBLE);
        } else if (!isPermissionsGranted) {
            tvStatus.setText(getString(R.string.text_permission_should_be_accepted));
            tvCompanyName.setVisibility(View.GONE);
            tvStatus.setVisibility(View.VISIBLE);
        }
    }
}
