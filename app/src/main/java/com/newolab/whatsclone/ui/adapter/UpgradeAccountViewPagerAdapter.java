package com.newolab.whatsclone.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.newolab.whatsclone.R;
import com.newolab.whatsclone.ui.fragment.UpgradePagerFragment;

public class UpgradeAccountViewPagerAdapter extends FragmentStatePagerAdapter {

    private static final int NUM_PAGES = 2;

    public UpgradeAccountViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return UpgradePagerFragment.newInstance(R.string.text_upgrade_password_lock, R.drawable.ic_fingerprint_white);
            case 1:
                return UpgradePagerFragment.newInstance(R.string.text_upgrade_remove_ads, R.drawable.ic_block_white);
            default:
                return new UpgradePagerFragment();

        }
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
