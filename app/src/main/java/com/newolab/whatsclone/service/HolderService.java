package com.newolab.whatsclone.service;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.newolab.whatsclone.helper.ChromeClient;
import com.newolab.whatsclone.helper.WebClient;
import com.newolab.whatsclone.WhatsClone;

public class HolderService extends Service {
    private static HolderService mHolderService;
    private WebView webView;

    public static synchronized HolderService newInstance() {
        HolderService holderService;
        synchronized (HolderService.class) {
            if (mHolderService == null) {
                mHolderService = new HolderService();
            }
            holderService = mHolderService;
        }
        return holderService;
    }

    public WebView initWebView(Activity context) {
        if (this.webView != null) {
            return this.webView;
        }
        this.webView = null;
        WebView webView = new WebView(context);
        webView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        webView.setWebChromeClient(new ChromeClient());
        webView.setWebViewClient(new WebClient());

        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setBlockNetworkImage(false);
        webView.getSettings().setBlockNetworkLoads(false);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setNeedInitialFocus(false);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        webView.getSettings().setAppCacheMaxSize(5242880);
        webView.getSettings().setAppCachePath(WhatsClone.newInstance().getApplicationContext().getCacheDir().getAbsolutePath());
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setUserAgentString("Mozilla/5.0(X11;U;Linux i686;en-US;rv:40.0)Gecko/20100101 Firefox/40.0");
        webView.loadUrl("https://web.whatsapp.com");
        return webView;
    }

    public void setWebView(WebView webView) {
        Log.e("WHATSWEB", "HolderService setWebView");
        this.webView = webView;
    }

    public WebView getWebView() {
        Log.e("WHATSWEB", "HolderService getWebView");
        return this.webView;
    }

    public IBinder onBind(Intent intent) {
        Log.e("WHATSWEB", "onBind");
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Log.e("WHATSWEB", "onCreate");
    }
}
