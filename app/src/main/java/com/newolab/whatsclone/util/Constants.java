package com.newolab.whatsclone.util;

public class Constants {

    public static final String ADMOB_ID = "ca-app-pub-9032617439824601~2490222157";
    public static final String ADMOB_AD_UNIT_ID = "ca-app-pub-9032617439824601/4090244287";

    public static final long SPLASH_DISPLAY_LENGTH = 1000;

    public static final int SESSION_COUNTS_TO_RATE = 3;

    public static final String FEEDBACK_EMAIL = "zeynebmv71@gmail.com";

    public static final String IAP_SKU_PREMIUM_ACCOUNT = "j31_2018";

    public static final String PREFERENCE_NAME = "whatsweb_sp";
    public static final String PREFERENCE_KEY_IS_PRO = "whatsweb_is_pro";
    public static final String PREFERENCE_KEY_IS_ADS_REMOVED = "whatsweb_is_ads_removed";
}
