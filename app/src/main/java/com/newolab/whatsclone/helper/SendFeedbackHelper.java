package com.newolab.whatsclone.helper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.newolab.whatsclone.R;
import com.newolab.whatsclone.util.Constants;

public class SendFeedbackHelper {

    public static void sendViaMail(Activity activity) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", Constants.FEEDBACK_EMAIL, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.feedback_title));
        emailIntent.putExtra(Intent.EXTRA_TEXT, activity.getString(R.string.feedback_hint));
        activity.startActivity(Intent.createChooser(emailIntent, activity.getString(R.string.feedback_chooser_hint)));
    }
}
