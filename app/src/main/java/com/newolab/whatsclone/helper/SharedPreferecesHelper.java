package com.newolab.whatsclone.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.newolab.whatsclone.util.Constants;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferecesHelper {

    private static SharedPreferences getPreferenceReference(Context context) {
        return context.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
    }

    public static void saveBoolean(Context context, String key, Boolean value) {
        SharedPreferences.Editor editor = getPreferenceReference(context).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static Boolean getBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = getPreferenceReference(context);
        return sharedPreferences.getBoolean(key, false);
    }

    public static void setPro(Context context, Boolean isPro) {
        saveBoolean(context, Constants.PREFERENCE_KEY_IS_PRO, isPro);
    }

    public static Boolean isPro(Context context) {
        return getBoolean(context, Constants.PREFERENCE_KEY_IS_PRO);
    }

    public static void setAdsRemoved(Context context, Boolean isRemoved) {
        saveBoolean(context, Constants.PREFERENCE_KEY_IS_ADS_REMOVED, isRemoved);
    }

    public static Boolean isAdsRemoved(Context context) {
        return getBoolean(context, Constants.PREFERENCE_KEY_IS_ADS_REMOVED);
    }

    public static void setPro(Context context) {
        setPro(context, true);
        setAdsRemoved(context, true);
    }
}
