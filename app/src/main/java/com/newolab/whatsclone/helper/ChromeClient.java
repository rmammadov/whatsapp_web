package com.newolab.whatsclone.helper;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.newolab.whatsclone.ui.MainActivity;

public class ChromeClient extends WebChromeClient {

    public ChromeClient() {
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        Log.e("WHATSWEB", consoleMessage.message());
        return super.onConsoleMessage(consoleMessage);
    }

    @SuppressLint({"NewApi"})
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        return MainActivity.getInstance().startFileChooserIntent(filePathCallback, fileChooserParams.createIntent());
    }

    @RequiresApi(api = 21)
    public void onPermissionRequest(PermissionRequest request) {
        request.grant(request.getResources());
    }

    @Override
    public void onPermissionRequestCanceled(PermissionRequest request) {
        super.onPermissionRequestCanceled(request);
    }
}
