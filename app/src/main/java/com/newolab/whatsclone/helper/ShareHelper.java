package com.newolab.whatsclone.helper;

import android.app.Activity;
import android.content.Intent;

import com.newolab.whatsclone.R;
import com.newolab.whatsclone.util.PackageNameUtil;

public class ShareHelper {

    public static void shareApp(Activity activity) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getShareText(activity));
        sendIntent.setType("text/plain");
        activity.startActivity(Intent.createChooser(sendIntent, activity.getString(R.string.share_chooser_hint)));
    }

    private static String getShareText(Activity activity) {
        return activity.getString(R.string.share_text) + " " + "Download: " + PackageNameUtil.getPackageName(activity);
    }
}
