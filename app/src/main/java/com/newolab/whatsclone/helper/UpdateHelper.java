package com.newolab.whatsclone.helper;

import android.app.Activity;

import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.enums.Display;
import com.newolab.whatsclone.R;

public class UpdateHelper {

    public static void checkUpdate(Activity activity) {
        new AppUpdater(activity)
                .setDisplay(Display.DIALOG)
                .setDisplay(Display.NOTIFICATION)
                .setTitleOnUpdateAvailable(activity.getString(R.string.update_title))
                .setContentOnUpdateAvailable(activity.getString(R.string.update_content))
                .setTitleOnUpdateNotAvailable(activity.getString(R.string.update_not_available_title))
                .setContentOnUpdateNotAvailable(activity.getString(R.string.update_not_available_content))
                .setButtonUpdate(activity.getString(R.string.update_button))
                .setButtonDismiss(activity.getString(R.string.update_dismiss_button))
                .setButtonDoNotShowAgain(activity.getString(R.string.update_dont_show_button))
                .setIcon(R.mipmap.ic_launcher) // Notification icon
                .setCancelable(false) // Dialog could not be dismissable
                .start();
    }
}
