package com.newolab.whatsclone.helper;

import android.app.Activity;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.newolab.whatsclone.R;
import com.newolab.whatsclone.util.Constants;
import com.newolab.whatsclone.util.PackageNameUtil;

public class FeedbackHelper {

    public static void showFeedbackDialogBasedSessions(Activity activity) {
        final RatingDialog ratingDialog = new RatingDialog.Builder(activity)
                .icon(activity.getResources().getDrawable(R.mipmap.ic_launcher))
                .session(Constants.SESSION_COUNTS_TO_RATE)
                .threshold(3)
                .title(activity.getString(R.string.rate_title))
                .titleTextColor(R.color.black)
                .positiveButtonText(activity.getString(R.string.rate_positive_button))
                .negativeButtonText(activity.getString(R.string.rate_negative_button))
                .positiveButtonTextColor(R.color.colorAccent)
                .negativeButtonTextColor(R.color.grey_500)
                .formTitle(activity.getString(R.string.rate_form_title))
                .formHint(activity.getString(R.string.rate_form_hint))
                .formSubmitText(activity.getString(R.string.rate_form_submit_button))
                .formCancelText(activity.getString(R.string.rate_form_cancel_button))
                .ratingBarColor(R.color.colorAccent)
                .playstoreUrl(getAppURL(activity))
                .onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared) {

                    }
                })
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {

                    }
                }).build();

        ratingDialog.show();
    }

    public static void showFeedbackDialogInstant(Activity activity) {
        final RatingDialog ratingDialog = new RatingDialog.Builder(activity)
                .icon(activity.getResources().getDrawable(R.mipmap.ic_launcher))
                .threshold(3)
                .title(activity.getString(R.string.rate_title))
                .titleTextColor(R.color.black)
                .positiveButtonText(activity.getString(R.string.rate_positive_button))
                .negativeButtonText(activity.getString(R.string.rate_negative_button))
                .positiveButtonTextColor(R.color.colorAccent)
                .negativeButtonTextColor(R.color.grey_500)
                .formTitle(activity.getString(R.string.rate_form_title))
                .formHint(activity.getString(R.string.rate_form_hint))
                .formSubmitText(activity.getString(R.string.rate_form_submit_button))
                .formCancelText(activity.getString(R.string.rate_form_cancel_button))
                .ratingBarColor(R.color.colorAccent)
                .playstoreUrl(getAppURL(activity))
                .onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared) {

                    }
                })
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {

                    }
                }).build();

        ratingDialog.show();
    }

    private static String getAppURL(Activity activity){
        return PackageNameUtil.getPackageName(activity);
    }
}
