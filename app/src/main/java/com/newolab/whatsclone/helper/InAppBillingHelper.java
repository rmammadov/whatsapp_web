package com.newolab.whatsclone.helper;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.newolab.whatsclone.util.Constants;

import java.util.List;

public class InAppBillingHelper implements PurchasesUpdatedListener, ConsumeResponseListener {

    public InAppBillingCallback mCallback;

    public interface InAppBillingCallback {
        public void onPremiumPurchased(boolean status);
    }

    private BillingClient mBillingClient;

    public void setupBillingConnection(final Activity activity, final Boolean newPurchase) {
        mBillingClient = BillingClient.newBuilder(activity).setListener(this).build();

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {
                if (responseCode == BillingClient.BillingResponse.OK) {
                    if (newPurchase) {
                        BillingFlowParams.Builder builder = BillingFlowParams.newBuilder().setSku(Constants.IAP_SKU_PREMIUM_ACCOUNT).setType(BillingClient.SkuType.INAPP);
                        int response = mBillingClient.launchBillingFlow(activity, builder.build());
                    } else {
                        getPurchaseHistory();
                    }
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

            }
        });
    }

    private void getPurchaseHistory() {
        mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP,
                new PurchaseHistoryResponseListener() {
                    @Override
                    public void onPurchaseHistoryResponse(@BillingClient.BillingResponse int responseCode,
                                                          List<Purchase> purchasesList) {
                        if (responseCode == BillingClient.BillingResponse.OK
                                && purchasesList != null) {
                            for (Purchase purchase : purchasesList) {
                                handlePurchase(purchase);
                            }
                        }
                    }
                });

    }

    private void handlePurchase(Purchase purchase) {
        switch (purchase.getSku()) {
            case Constants.IAP_SKU_PREMIUM_ACCOUNT:
                mCallback.onPremiumPurchased(true);
                mBillingClient.consumeAsync(purchase.getPurchaseToken(), this);
                break;
        }
    }

    public void endConnection() {
        try {
            if (mBillingClient != null) {
                mBillingClient.endConnection();
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Caught an IllegalArgumentException..." + e.getMessage());
        }
    }

    @Override
    public void onConsumeResponse(int responseCode, String purchaseToken) {
        if (responseCode == BillingClient.BillingResponse.OK) {
            endConnection();
        }
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (responseCode == BillingClient.BillingResponse.OK && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
            mCallback.onPremiumPurchased(false);
        } else {
            mCallback.onPremiumPurchased(false);
        }
    }
}
