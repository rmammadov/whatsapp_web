package com.newolab.whatsclone.helper;

import android.util.Base64;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.newolab.whatsclone.WhatsClone;
import com.newolab.whatsclone.service.HolderService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class WebClient extends WebViewClient {
    public WebClient() {}

    private void startLoadingCustomDesign() {
        try {
            InputStream open = WhatsClone.newInstance().getAssets().open("s.css");
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            HolderService.newInstance().getWebView().loadUrl("javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('style');style.type = 'text/css';style.innerHTML = window.atob('" + Base64.encodeToString(bArr, 2) + "');parent.appendChild(style)})();", null);
            InputStream open2 = WhatsClone.newInstance().getAssets().open("site.js");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(open2));
            String str = "";
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    str = str + readLine;
                } else {
                    open2.close();
                    HolderService.newInstance().getWebView().loadUrl("javascript:document.getElementById('pane-side').addEventListener('click',function f(e) {setTimeout(function(){document.getElementsByClassName('_1Iexl')[1].style.flexBasis='100%';document.getElementsByClassName('k1feT')[1].style.flexBasis='0%';}, 400);})()", null);
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        Log.e("WHATSWEB", "CustomWebViewClient onPageFinished");
        startLoadingCustomDesign();
    }
}
