package com.newolab.whatsclone;

import android.app.Application;

import com.github.omadahealth.lollipin.lib.managers.LockManager;
import com.google.android.gms.ads.MobileAds;
import com.newolab.whatsclone.ui.SetPinActivity;
import com.newolab.whatsclone.util.Constants;
import com.onesignal.OneSignal;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;

public class WhatsClone extends Application {
    private static WhatsClone mWhatsWeb;

    public static synchronized WhatsClone newInstance() {
        WhatsClone whatsWeb;
        synchronized (WhatsClone.class) {
            if (mWhatsWeb == null) {
                mWhatsWeb = new WhatsClone();
            }
            whatsWeb = mWhatsWeb;
        }
        return whatsWeb;
    }

    public void onCreate() {
        super.onCreate();
        mWhatsWeb = this;
        InternetAvailabilityChecker.init(this);
        enableLock();
        initAds();
        initOneSignal();
    }

    private void initAds() {
        MobileAds.initialize(this,
                Constants.ADMOB_ID);
    }

    private void enableLock() {
        LockManager<SetPinActivity> lockManager = LockManager.getInstance();
        lockManager.enableAppLock(this, SetPinActivity.class);
    }

    private void initOneSignal() {
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }
}
