var show_main_window = function () {
    document.getElementById('main').style.width='100%';document.getElementById('side').style.display='none';
};

var show_chat_list = function () {
    document.getElementById('side').style.display='';document.getElementById('main').style.width='0%';
    document.getElementsByClassName('_3q4NP k1feT')[0].innerHTML='';
};
var addClickListener = function() {
    var list_items = document.getElementsByClassName('infinite-list-item');
    for (var i=0; i<list_items.length; i++) {
        list_items[i].addEventListener('click', show_main_window, false);
    }
};
var observeDOM = (function() {
    addClickListener();

    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
        eventListenerSupported = window.addEventListener;

    return function(obj, callback){
        if( MutationObserver ) {
            // define a new observer
            var obs = new MutationObserver(function(mutations, observer){
                var length = mutations[0].addedNodes.length;
                if(length)
                    callback(mutations[0].addedNodes[length-1]);
            });
            // have the observer observe foo for changes in children
            obs.observe( obj, { childList:true, subtree:true });
        } else if( eventListenerSupported ){
//            obj.addEventListener('DOMNodeInserted', callback(null), false);
            addClickListener();
        }
    };
})();

// Observe a specific DOM element:
observeDOM( document.getElementById('pane-side'), function(addedNode) {
    debugger;
    if (addedNode) {
        var nodeClass = addedNode.className;
        if (nodeClass == 'infinite-list-item infinite-list-item-transition'
                || nodeClass == "matched-text") {
            addedNode.addEventListener('click', show_main_window, false);
            console.log('event added');
        }
    }
});